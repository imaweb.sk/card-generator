<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Card Generator</title>
	<link 
		rel="icon" 
		type="image/png" 
		href="https://imaweb.sk/img/favicon.png" />
	<link 
		rel="stylesheet"
		href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
		integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
		crossorigin="anonymous">
	<script 
		src="https://kit.fontawesome.com/dd1ff516c9.js" 
		crossorigin="anonymous"
	></script>
	<?php
		$card = $keys = $values = [];
		for ($i=0; $i<150; $i++) { 
			array_push($keys, substr(str_shuffle("0123456789"."0123456789"."0123456789"."0123456789"), 0, 4));
			array_push($values, substr(str_shuffle("0123456789"."0123456789"."0123456789"."0123456789"."0123456789"."0123456789"), 0, 6));
		}
		$keys = array_unique($keys);
		$keys = array_flip(array_slice($keys, 0, 132));
		ksort($keys);
		$values = array_unique($values);
		$counter = 0;
		foreach ($keys as $key => $value) {
			$card[$key] = $values[$counter];
			$counter++;
		}
		$card = array_chunk(array_chunk($card, 11, true), 4, true);
	?>
</head>
<body>
	<div class="bg-warning pt-4 pb-4">
		<div class="container">
			<div class="text-center mb-3 h1">
				<i class="fas fa-exclamation-triangle"></i>
			</div>
			<p class="mb-2 text-white text-dark text-center">
				<b>
					YOU WANT TO READ ME VERY, VERY CAREFULLY
				</b>
			</p>
			<p class="mb-0 text-white text-dark text-center">
				This is just a hobby project that has absolutely nothing to do with <u><a href="https://www.nemid.nu/dk-en/" class="text-dark">NemID</u>!</a>
			</p>
			<p class="mb-0 text-white text-dark text-center">
				You can use it to learn more about PHP and how to work with arrays.
			</p>
			<p class="mb-0 text-white pb-2 text-dark text-center">
				Nothing more, nothing less. Code is available <a href="https://gitlab.com/imaweb.sk/card-generator/-/blob/master/index.php" class="text-white">here</a>.
			</p>
		</div>	
	</div>
	<div class="container mt-5 text-center">
		<?php foreach($card as $section) { ?>		
			<section class="mb-5">
				<?php foreach($section as $column) { ?>
					<table class="d-inline m-2">
						<tr class="text-left">
							<td class="text-center">
								<i class="fas fa-hashtag"></i>
							</td>
							<td class="text-center">
								<i class="fas fa-key"></i>
							</td>
						</tr>
						<?php foreach($column as $key => $value) { ?>
							<tr>
								<td class="pr-1">
									<b>
										<?php echo $key; ?>
									</b>
								</td>
								<td>
									<?php echo $value; ?>
								</td>
							</tr>
						<?php } ?>
					</table>
				<?php } ?>
			</section>	
		<?php } ?>
	</div>

</body>
</html>